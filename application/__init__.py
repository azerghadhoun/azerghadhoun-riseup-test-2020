from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
from flasgger import Swagger
from ngrok.flask_ngrok import run_with_ngrok


db = SQLAlchemy()

swagger_config = {
    "headers": [
    ],
    "specs": [
        {
            "endpoint": 'apispec',
            "route": '/apispec.json',
            "rule_filter": lambda rule: True,
            "model_filter": lambda tag: True,
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/"
}


def create_app():
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object("config.Config")

    db.init_app(app)

    with app.app_context():
        from . import routes
        db.create_all()
        db.app = app
        scheduler = APScheduler()
        scheduler.init_app(app)
        scheduler.start()
        return app


app = create_app()
run_with_ngrok(app)
swagger = Swagger(app, config=swagger_config)
