from flask import current_app as app
from flask import jsonify

from .models import UserTraining


@app.route("/api/v1/user", methods=["GET"])
def get_users():
    """
    ---
    definitions:
        users:
            type: array
            items:
                type: string
                description: users_name
    responses:
      200:
        description: A list of objects containing users
        schema:
          $ref: '#/definitions/users'
    """

    user_trainings = UserTraining.query.filter(UserTraining.user.isnot(None))
    users = [ut.user for ut in user_trainings]
    return jsonify(users)


@app.route("/api/v1/user/average", methods=["GET"])
def get_users_average():
    """
    ---
    definitions:
        users_averages:
            type: array
            items:
                type: object
                properties:
                    users_name:
                        type: string
                    average:
                        type: number
    responses:
      200:
        description: A list of objects containing a the average trainings for each user
        schema:
          $ref: '#/definitions/users_averages'
    """

    user_trainings = UserTraining.query.filter(UserTraining.user.isnot(None))
    response = [{
            "users_name": ut.user,
            "average": ut.error_message or ut.average
    } for ut in user_trainings]
    return jsonify(response)


@app.route("/api/v1/user/<string:id>", methods=["GET"])
def get_user_data(id):
    """
    ---
    parameters:
      - name: id
        in: path
        type: string
        required: true
    definitions:
      user_training:
        type: object
        properties:
          users_name:
            type: string
          startdate:
            type: string
          enddate:
            type: string
          nbr_training:
            type: integer
          average:
            type: number
    responses:
      200:
        description: An object containing training information for a specific user
        schema:
          $ref: '#/definitions/user_training'
    """
    user_training = UserTraining.query.filter(UserTraining.user == id).first()
    response = {}
    if user_training:
        response = {
            "users_name": user_training.user,
            "startdate": user_training.start_date,
            "enddate": user_training.end_date,
            "nbr_training": user_training.nb_trainings,
            "average": user_training.average
        }
    return jsonify(response)



