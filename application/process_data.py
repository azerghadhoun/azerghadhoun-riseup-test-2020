import pandas as pd
import math
from datetime import datetime
import os
from . import db
from .models import UserTraining


def _process_data():
    path = os.path.dirname(os.path.abspath(__file__))
    filename = 'input.xlsx'
    filepath = os.path.join(path, 'input_data/'+filename)
    df = pd.read_excel(
        filepath,
        # use only the first 4 columns
        usecols=list(range(4)),
        engine='openpyxl'
    )

    def _get_date_value(date):
        if isinstance(date, datetime):
            return date
        if isinstance(date, str):
            try:
                return datetime.strptime(date, "%m/%d/%Y")
            except ValueError as err:
                pass
        return math.nan

    def _compute_diff(row):
        start = row['start']
        end = row['end']
        start = _get_date_value(start)
        if isinstance(start, float) and math.isnan(start):
            return math.nan
        end = _get_date_value(end)
        if isinstance(end, float) and math.isnan(end):
            return math.nan
        return (end - start).days

    df['diff'] = df.apply(_compute_diff, axis=1)

    def _compute_result(row):
        user = row['user']
        if not user or (isinstance(user, float) and math.isnan(user)):
            return '404, user not found'
        nb_trainings = row['nb_trainings']
        diff = row['diff']
        if isinstance(nb_trainings, (float, int)):
            if math.isnan(nb_trainings):
                return '404, null nb_trainings'
            if nb_trainings == 0:
                # division by zero
                return '404, null nb_trainings'
            if diff < 0:
                return '404, neg diff'
            if diff == 0:
                return '404, null diff'
            result = diff / nb_trainings
            if result < 0:
                return '404, neg result'
            return result
        if isinstance(nb_trainings, str) and nb_trainings.isnumeric():
            return diff / float(nb_trainings)
        return '404, null result'

    df['result'] = df.apply(_compute_result, axis=1)

    def _insert_in_db(row):
        row_nbr = row.name
        user_training = UserTraining.query.filter(
            UserTraining.row_nbr == row_nbr and UserTraining.filename == filename,
        ).first()
        if user_training:
            return
        data = {
            'user': row['user'],
            'start_date': row['start'],
            'end_date': row['end'],
            'nb_trainings': row['nb_trainings'],
            'diff': row['diff'],
            'filename': filename,
            'row_nbr': row_nbr,
        }
        if isinstance(row['result'], (float, int)):
            data['average'] = row['result']
        else:
            data['error_message'] = row['result']
        new_user_training = UserTraining(**data)
        db.session.add(new_user_training)
        db.session.commit()

    df.apply(_insert_in_db, axis=1)


