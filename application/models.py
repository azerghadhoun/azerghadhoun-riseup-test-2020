"""Data models."""
from . import db


class UserTraining(db.Model):

    __tablename__ = "user_training"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(128), nullable=True)
    start_date = db.Column(db.Date, nullable=True)
    end_date = db.Column(db.Date, nullable=True)
    nb_trainings = db.Column(db.Integer, nullable=True)
    diff = db.Column(db.Integer, nullable=True)
    average = db.Column(db.Integer, nullable=True)
    error_message = db.Column(db.String(64), nullable=True)
    filename = db.Column(db.String(64), nullable=False)
    row_nbr = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return "<User {}>".format(self.user)
