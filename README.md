Project dependencies:

* [python3](https://www.python.org/downloads/)

* [python-pip](https://pip.pypa.io/en/stable/installing/)

Clone the project on your local machine
```sh
git clone git@bitbucket.org:azerghadhoun/azerghadhoun-riseup-test-2020.git
```
Browse to the project directory
```sh
cd azerghadhoun-riseup-test-2020
```
Setup the dependencies
```sh
sudo pip install -r requirements.txt
```
Run the app:
```sh
python app.py
```
Once the web app is running you can browse the api documentation
via the link http://localhost:5000

You can also browse api documentation via ngrok
 (please make sure to replace <ngrok_id> with the ngrok_id that shows in your terminal)

> https://<ngrok_id>.ngrok.io

As soon as the program runs, the data processing is done in the background,
and probably take some seconds to be visible through the following endpoints:

This endpoint shows a list of valid users extracted from the input csv
> https://<ngrok_id>.ngrok.io/api/v1/user

The format of the data would be:
```python
[
    "users_name1",
    "users_name2",
    "users_name3",
    ...
]
```

This endpoint shows data relative to the user using its ID 
(please replace <id> in the following link with one of the users id from the previous endpoint results)
> https://<ngrok_id>.ngrok.io/api/v1/user/<id>

The format of the data would be:
```python
{
    "users_name": "user",
    "startdate": "startdate", 
    "enddate": "enddate",
    "nbr_training": "nbr_training", 
    "average": "average_time"
}
```

This endpoint shows the average days of trainings per user
> https://<ngrok_id>.ngrok.io/api/v1/user/average

The format of the data would be:
```python
[
    {
        "users_name": "user",
        "average": "average_time"
    },
    {
        "users_name": "user2",
        "average": "average_time2"
    },
    ...
]
```


To run the unit tests, you can use the following command line:

```sh
python test.py
```



