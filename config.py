from application.process_data import _process_data
from datetime import datetime


class Config:

    # General Config
    SECRET_KEY = 'IUFNO8IunCoy5eofHy2enofyd8slili57v6gRg5'
    FLASK_ENV = 'development'

    # Database
    SQLALCHEMY_DATABASE_URI = 'sqlite:///riseup.sqlite3'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    JOBS = [
        {
            'id': 'process_data',
            'func': _process_data,
            'trigger': 'date',
            'run_date': datetime.now()
        }
    ]

    SCHEDULER_API_ENABLED = True
