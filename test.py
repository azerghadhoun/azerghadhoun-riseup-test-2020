import os
import unittest
from application import app, db
from application.process_data import _process_data
from application.models import UserTraining
TEST_DB = 'test.db'


class BasicTests(unittest.TestCase):

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
                                                os.path.join(os.path.dirname(os.path.abspath(__file__)), TEST_DB)
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        _process_data()

        # Disable sending emails during unit testing
        self.assertEqual(app.debug, False)

    def tearDown(self):
        pass

    def test_dummy_page(self):
        response = self.app.get('/dummy', follow_redirects=True)
        # Main page is not found
        self.assertEqual(response.status_code, 404)

    def test_swagger_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_process_data_count(self):
        # Make sur the records inserted in database from the file input.xlsx
        # is equal to the number of lines of the same file
        count = UserTraining.query.filter(UserTraining.filename == 'input.xlsx').count()
        self.assertEqual(count, 48)

    def test_process_data_validity(self):
        # Make sur the records inserted in database from the file input.xlsx
        # are valid
        users_trainings = UserTraining.query.limit(5).all()
        for user_training in users_trainings:
            end_date = user_training.end_date
            start_date = user_training.start_date
            nb_trainings = user_training.nb_trainings
            average = user_training.average
            error_message = user_training.error_message
            if end_date and start_date:
                diff = (end_date - start_date).days
                self.assertEqual(diff, user_training.diff)
                if nb_trainings and nb_trainings != 0:
                    if diff / nb_trainings < 0:
                        self.assertLess(average, 0)
                    elif diff / nb_trainings > 0:
                        self.assertGreater(average, 0)
                elif nb_trainings == 0:
                    self.assertEqual(average, None)
                    self.assertEqual(error_message, '404, null nb_trainings')


if __name__ == "__main__":
    unittest.main()
